import 'package:teste_slidy/src//component/components_bloc.dart';
import 'package:teste_slidy/src/second/second-page_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:teste_slidy/src/app_widget.dart';
import 'package:teste_slidy/src/app_bloc.dart';

class AppModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc((i) => ComponentsBloc()),
        Bloc((i) => SecondPageBloc()),
        Bloc((i) => AppBloc()),
      ];

  @override
  List<Dependency> get dependencies => [];

  @override
  Widget get view => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
