import 'package:flutter/material.dart';

class ComponentsWidget {

  Widget logo(){
    return Padding(
      padding: EdgeInsets.only(top: 50, bottom: 50),
      child: Center(
        child: CircleAvatar(
          child: Text('M', style: TextStyle(fontSize: 20))),
    ));
  }

  Widget inputUserName(){
    return Padding(
      padding: EdgeInsets.only(left: 30, right: 30, bottom: 30),
      child: TextField(
        decoration: InputDecoration(
          labelText: 'Nome do Usuário',
          border: OutlineInputBorder()
        ),
      ),
    );
  }

  Widget inputPassword(){
    return Padding(
      padding: EdgeInsets.only(left: 30, right: 30, bottom: 30),
      child: TextField(
        decoration: InputDecoration(
          labelText: 'Senha',
          border: OutlineInputBorder()
        ),
        obscureText: true,
      ),
    );
  }

  Widget forgetPassword(){
    return Center(
      child: Text('Esqueceu Sua Senha?', style: TextStyle(fontSize: 14)));
  }

  Widget buttonLogin({Function onTap}){
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.all(30),
        child: Center(
          child: Container(
            height: 50,
            width: 250,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(30)
            ),
            child: Center(
              child: Text('LOGIN', style: TextStyle(color: Colors.white))),
          ),
        ),
      ),
    );
  }

}

