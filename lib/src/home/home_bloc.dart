import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:teste_slidy/src/home/home_module.dart';

class HomeBloc extends BlocBase {

  HomeBloc();

  bool isLoading = false;

  factory HomeBloc.get() => HomeModule.to.bloc<HomeBloc>();

  changeLoading(){
    isLoading = !isLoading;
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
