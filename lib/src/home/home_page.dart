
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teste_slidy/src/home/home_bloc.dart';
import 'package:teste_slidy/src/home/home_module.dart';
import 'package:teste_slidy/src/second/second-page_page.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slidy Study"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => SecondPage()));
            },
          )
        ],
      ),
      body: ConsumerModule<HomeBloc, HomeModule>(
        builder: (BuildContext context, HomeBloc value) {

          return GestureDetector(
            onTap: HomeBloc.get().changeLoading,
            child: Center(
              child: AnimatedContainer(
                duration: Duration(milliseconds: 200),
                width: value.isLoading ? 50 : 250,
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  gradient: LinearGradient(colors: <Color>[Colors.blue[700], Colors.blue[300]]
                  ),
                  borderRadius: BorderRadius.circular(value.isLoading ? 50 : 10)
                ),
                child: Center(
                  child: value.isLoading ?
                    CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white))
                    :
                    Center(
                     child: Text('Login', style: TextStyle(color: Colors.white)),
                    ),
                )
              ),
            ),
          );  

        },
      ),
    );
  }
}
