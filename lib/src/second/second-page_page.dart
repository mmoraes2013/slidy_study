import 'package:flutter/material.dart';
import 'package:teste_slidy/src/component/components_widget.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();

}

class _SecondPageState extends State<SecondPage> with ComponentsWidget, SingleTickerProviderStateMixin {

  AnimationController animacao;
  Animation<double> nomeUsuario;
  Animation<double> senhaUsuario;

  _onTap(){
    animacao.forward();
  }

  Widget _column(){
    return Column(
      children: <Widget>[
        logo(),
        AnimatedBuilder(
          animation: nomeUsuario, 
          child: inputUserName(),
          builder: (BuildContext context, Widget child) {
            return Transform.translate(offset: Offset(nomeUsuario.value, 0), child: child);
          },
        ),
        AnimatedBuilder(
          animation: senhaUsuario, 
          child: inputPassword(),
          builder: (BuildContext context, Widget child) {
            return Transform.translate(offset: Offset(senhaUsuario.value, 0), child: child);
          },
        ),        
        forgetPassword(),
        buttonLogin(onTap: _onTap),
      ],
    );
  }


  @override
  void initState() {
    super.initState();
    animacao = AnimationController(duration: Duration(milliseconds: 5000), vsync: this);

    nomeUsuario = Tween<double>(begin: 0, end: 350).animate(animacao);
    senhaUsuario = Tween<double>(begin: 0, end: -350).animate(animacao);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Page"),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          _column(),
        ],
      )
    );
  }
}
